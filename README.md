Wikipedia analytics for museums
===============================

A minimal set of scripts to create analytical data about Wikipedia pageviews starting from a root category.

It's based on the pagecounts archives available here: <http://dumps.wikimedia.org/other/pagecounts-ez/merged/>.
