#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright 2013 Stefano Costa <steko@iosa.it>

import logging
import requests


class MediaWikiAPIClient():

    def __init__(self, wiki):
        self.wiki = wiki
        logging.info('Initialized MediaWikiAPIClient for wiki {}'.format(wiki,))
        self.pages = []

    def get_category_members(self, category):
        '''Gets category members from MediaWiki API.'''

        params = {
            'action': 'query',
            'format': 'json',
            'list': 'categorymembers',
            'cmtitle': category,
            'cmlimit': 500,
            }

        url = "http://{}/w/api.php".format(self.wiki,)

        logging.info('Requesting URL {}'.format(url,))
        r = requests.get(url, params=params)

        result =  r.json()
        for page in result['query']['categorymembers']:
            logging.debug('Adding page id {}'.format(page['pageid'],))
            self.pages.append(page['title'])
            if page['ns'] == 14:
                self.get_category_members(page['title'])
                logging.info('Total pages: {}'.format(len(self.pages)))


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    mw = MediaWikiAPIClient(wiki='it.wikipedia.org')
    mw.get_category_members(u"Categoria:Siti archeologici d'Italia")
